
simbad_thumbnail() parameters
=============================

Parameters of the function simbad_thumbnail:

.. code-block:: python


    simbad_thumbnail(GAL, tab_gal='simbad', size_image_phy=100,
                     limtvel=500,  ned_plot='no', width = 300, height = 300,
                     labeltxt = 'off', xtpos=10, ytpos=20, deltay=10, miny=20,
                     minx=20):
    """
    Perform query in simbad database looking for neiborgh galaxies.

    Parameters:
    ------------
    - gal: Galaxy name (str type).
    - tab_gal: Input table (pandas DataFrame or equivalent) or "simbad" to
               download the galaxy data from Simbad database.
    - size_image_phy: Physical size of the image in kpc unit (default: 50).
    - limtvel: Maximum delta velocity limit (km/s) of the radial velocity for
               the companion  galaxies (default: 500).
    - ned_plot: Perform the same query in NED database (default: 'no')
    - width: Image width in pix (default: 300).
    - height: Image height in pix (default: 300).
    - labeltxt: activate labels (default: 'off')
    - xtpos: X-coordinate position in pix for label object offset (default: 10).
    - ytpos: Y-coordinate position in pix for label object offset (default: 20).
    - deltay: Delta Y-position for labels of object with close X-coordinates
              (default: 10).
    - miny: Minimum separation in Y-coordinates for activate the deltay
            (default: 20).
    - minx: Minimum separation in X-coordinates for activate the deltay
            (default: 20).

    Returns:
    --------
    None
    """
