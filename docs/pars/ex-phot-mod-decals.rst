ex_phot_mod_decals() parameters
===============================

Parameters of the function ex_phot_mod_decals:

.. code-block:: python


    ex_phot_mod_decals (gal, tab_gal='simbad', size_image_phy=50, band='r',
                         snr = 2.0, area_min=10, deblend='off',
                         area_min_deblend=20,
                         sky_box=[100, 100], sky_method='masking_sources',
                         nsigma=2, npixels=5, dilate_size=11,
                         mask_stars='no', fwhm=3.0, threshold=10, roundlo=-0.2,
                         roundhi=0.2, sharplo=0.2, aper_stars=5, aper_center=10,
                         flag_area_th=3, flag_SN_th=3, perc_SN_flag=50,
                         segmap=None, petro_extent_cas=1.5, eta=0.2,
                         run_auto='no', show_plot='yes', plot_model='yes',
                         sizet=20, outputfile='properties.dat', dr=10,
                         simbad_cone=2/3600.):
     """
         Perform photometric modeling on galaxy images by using Legacy-data

         Parameters:
         ------------
         - gal: Galaxy name (str type).
         - tab_gal: input table (pandas DataFrame or equivalent) or "simbad" to
                    download the galaxy data from Simbad database
                    (default: 'simbad').
         - size_image_phy: Physical size of the image in kpc unit (default: 50).
         - band: Band for photometry (default: 'r').
         - dr: Data release of legacy survey (default: 10).
         - simbad_cone: region size of query region to get the psf value
                        in arcsec (default: 2/3600).
         - user_order: array-like with the sepmentantion maps IDs to be analyzed
                       (e.g, [1,2])
         - user_list: array-like with the nicknames of sepmentantion maps IDs to
                      be analyzed (e.g, ['a','b'])
         - snr: Signal-to-noise ratio threshold to detect sources (default: 2.0).
         - area_min: Minimum area for object detection in kpc unit (default: 10).
         - deblend: Deblending option ('off' or 'on', default: 'off').
         - area_min_deblend: Minimum detection area for deblending in kpc unit
                             (default: 20).
         - sky_box: Sky box size for background estimation in pix unit
                    (default: [100, 100]).
         - sky_method: Sky estimation method ('masking_sources' or 'none',
                       default: 'masking_sources').
         - nsigma: Sigma level for object detection (default: 2).
         - npixels: Number of connected pixels for object detection (default: 5).
         - dilate_size: Size for dilating the segmentation map (default: 11).
         - mask_stars: Mask stars in the image ('yes' or 'no', default: 'no').
         - fwhm: Full-width at half-maximum for PSF estimation (default: 3.0).
         - threshold: Threshold for object (star) detection (default: 10).
         - roundlo: Lower limit for object roundness (default: -0.2).
         - roundhi: Upper limit for object roundness (default: 0.2).
         - sharplo: Lower limit for object sharpness (default: 0.2).
         - aper_stars: Aperture size for star mask (default: 5).
         - aper_center: Aperture size for center mask (default: 10).
         - flag_area_th: The threshold ratio between the object and PSF areas for
                         flagging (default: 3).
         - flag_SN_th: The threshold ratio between the signal pixel at a given
                       percentile (default: perc_SN_flag=50) and the background
                       value for flagging (default: 3).
         - perc_SN_flag: The percentile used to calculate the pixel signal of the
                         object for comparison with the background value
                         (default: 50).
         - segmap: Pre-existing segmentation map (str, default: None).
         - eta: The mean isophotal brightness to the mean brightness in
                an aperture ratio to define the Petrosian radius  (default: 0.2).
         - petro_extent_cas: Petrosian radius extent for CAS parameters
                             (default: 1.5).
         - sizet: Labe size to print Filed_view/Unwinding (default: 20).
         - run_auto: Run automaticaly phot_mod ('yes' or 'no', default: 'no').
         - show_plot: Display plots ('yes' or 'no', default: 'yes').
         - plot_model: Plot the galaxy model ('yes' or 'no', default: 'yes').
         - outputfile: Output file for saving properties
                       (default: 'properties.dat').

         Returns:
         --------
         None

         Examples:
         ```python
         # Example usage of ex_phot_mod function
         ex_phot_mod_decals(gal_data, galaxy_table, size_image_phy=50, band='R',
                         snr=2.0, area_min=10, deblend='off', area_min_deblend=20,
                         sky_box=[100, 100], sky_method='masking_sources',
                         nsigma=2, npixels=5, dilate_size=11,
                         mask_stars='no', fwhm=3.0, threshold=10, roundlo=-0.2,
                         roundhi=0.2, sharplo=0.2, aper_stars=5, aper_center=10,
                         flag_area_th=3, flag_SN_th=3, perc_SN_flag=50,
                         segmap=None, eta=0.2, petro_extent_cas=1.5,
                         env_info='no', region='circle', server=None, limtvel=500,
                         xtpos=10, ytpos=20, deltay=10, miny=20, simbadima='no',
                         field_size_phy=99, sizet=20,
                         run_auto='no', show_plot='yes', plot_model='yes',
                         outputfile='properties.dat')
         ```
     """
