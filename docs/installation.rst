
Installation
============

``The latest version of astromorphlib is`` `[1.0.10] <https://pypi.org/project/astromorphlib/>`_

The easiest way to install this package is via PyPI:

.. code:: bash

    pip install astromorphlib

If you prefer a manual installation, you can clone the
lastest version at `Gitlab repository <https://https://gitlab.com/joseaher/astromorphlib>`_:

.. code:: bash

    git clone https://gitlab.com/joseaher/astromorphlib.git

After the package download, go into folder:

.. code:: bash

    pip install .


Requirements
=============

``astromorphlib`` requires the following pacakge to run:

    * statmorph
    * splusdata
    * astro-datalab
    * astroplotlib
    * astropy
    * astroquery
    * photutils (1.5.0 <)**
    * numpy
    * scipy
    * matplotlib
    * wget
    * colorama

** to install this specific version of photutils you can try:

.. code:: bash

    pip install photutils==1.5.0

Resolving Compatibility Issues with `photutils` (==1.5.0) and Newer Versions of `numpy` (>= 2.0)
=====================================================================================

When using `photutils` with `numpy` version 2.0 or higher, you may encounter a 
compatibility issue. This occurs due to changes in how the `index_exp` function 
is imported. To resolve this, you need to update the import statement in 
the `background_2d.py` file.

Steps to Fix
------------

1. Locate the `background_2d.py` file:
   - This file is typically located in a path like:
     ::

       envs/astroconda/lib/python3.13/site-packages/photutils/background/background_2d.py

2. Open the file and find the following line of code:

   .. code-block:: python

       from numpy.lib.index_tricks import index_exp

3. Replace it with this updated import statement:

   .. code-block:: python

       from numpy.lib._index_tricks_impl import index_exp

4. Save the file.

5. Re-test your setup to ensure `photutils` works correctly with `numpy` 
version 2.0 and above.

---

By following these steps, you can resolve the issue and ensure compatibility 
with the latest `numpy` versions.


Uninstallation
=============

In order to uninstall ``astromorphlib`` just type on the terminal:

.. code:: bash

   pip uninstall astromorphlib
