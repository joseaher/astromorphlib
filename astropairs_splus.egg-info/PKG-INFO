Metadata-Version: 2.1
Name: astropairs-splus
Version: 0.1
Summary: Python scripts to analyze the morphology of interacting galaxies
Home-page: https://gitlab.com/joseaher/astropairs_splus
Author: J. A. Hernandez-Jimenez
Author-email: joseaher@gmail.com
License: UNKNOWN
Description: # astropairs_splus
        
        Python scripts to analyze the morphology of interacting galaxies. The package is
        designed to download S-PLUS images automatically (https://splus.cloud/). There are functions to
        calculate a 2D sky background of the images and deblended segmentation maps of
        interacting systems with merger isophotes. The non-parametric analysis is
        performed by using wrapper of the `statmorph` package (https://github.com/vrodgom/statmorph).  The user can download a
        table of the galaxies within Field-of-View of S-PLUS images from SIMBAD server
        (http://simbad.u-strasbg.fr/simbad/). There is a function to display DSS2 images
        (http://alasky.u-strasbg.fr/hips-image-services/hips2fits).  
        
        version: 0.1
        
        (c) 2021-2022 J. A. Hernandez-Jimenez
        
        E-mail: joseaher@gmail.com
        
        Website: https://gitlab.com/joseaher/astropairs_splus
        
        ## Installation
        
        astroplotlib requires:
        
            * statmoprh
            * splusdata
            * numpy
            * scipy
            * matplotlib
            * astropy
            * astroquery
        
        
        This version can be easily installed within Anaconda Enviroment via PyPI:
        
            % pip install astropairs_splus
        
        If you prefer to install astroplotlib manually, you can clone the developing
        version at https://gitlab.com/joseaher/astropairs_splus. In the directory this
        README is in, simply:
        
            % pip install .
        
        or,
        
            % python setup.py install
        
        ## Uninstallation
        
        To uninstall astropairs_splus, simply
        
            % pip uninstall astropairs_splus
        
Platform: UNKNOWN
Description-Content-Type: text/markdown
